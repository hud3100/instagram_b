Rails.application.routes.draw do
  
  get 'pages/show'
  
  devise_for :users
  
  devise_scope :user do
  root to: "devise/sessions#new"
  end
  


end